# LZW-RS

This library contains Rust implementation of LZW compression alorithm.

## Compilation

Pull this repository and run `cargo build --release`. 
`liblzw.so` will appear under `target/release`.
You need to copy this file whereever it is expected.

## Usage

There are only 2 functions exported right now.
Check them out in `src/lib.rs` file.

## Python wrapper

Checkout [here.](https://gitlab.com/Wilmhit/lzw-rs-py)

#### Licensed under GPLv2
