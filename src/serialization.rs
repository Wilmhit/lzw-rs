use bit::BitIndex;

struct BitVector {
    bit_iter: u8,
    vector: Vec<u8>,
}

impl BitVector {
    fn new() -> BitVector {
        BitVector {
            bit_iter: 8,
            vector: vec![0],
        }
    }

    fn from(v: Vec<u8>) -> BitVector {
        BitVector {
            bit_iter: 0,
            vector: v,
        }
    }

    fn push_bits(&mut self, from: usize, mut how_many: u8) {
        let mut already_wrote = 0;
        while how_many > 0 {
            if self.bit_iter == 0 {
                self.vector.push(0);
                self.bit_iter = 8;
            }
            let val = from.bit(already_wrote);
            self.vector
                .last_mut()
                .unwrap()
                .set_bit((8 - self.bit_iter).into(), val);
            already_wrote += 1;
            how_many -= 1;
            self.bit_iter -= 1;
        }
    }

    fn pop_bits(&mut self, mut how_many: u8) -> Option<usize> {
        if how_many as usize > usize::bit_length() {
            panic!("Trying to pop too many bits at once");
        };
        if how_many == 0 {
            panic!("You cannot pop 0 bits");
        }

        let mut already_wrote = how_many as usize;
        let mut to_return = 0usize;
        while how_many > 0 {
            if self.bit_iter == 8 {
                if self.vector.len() == 1 {
                    return None;
                }
                self.vector.pop();
                self.bit_iter = 0
            }
            let bit = self.vector.last().unwrap().bit((7 - self.bit_iter).into());
            to_return.set_bit(already_wrote - 1, bit);
            already_wrote -= 1;
            how_many -= 1;
            self.bit_iter += 1;
        }
        Some(to_return)
    }

    fn finish_writing(&mut self, bitness: u8) {
        self.vector.push(bitness);
        self.vector.push(self.bit_iter);
    }

    fn read_bitness(&mut self) -> u8 {
        self.bit_iter = self.vector.pop().unwrap();
        self.vector.pop().unwrap()
    }
}

pub(crate) fn serialize(indexes: Vec<usize>) -> Vec<u8> {
    let max_value: usize = *indexes.iter().max().unwrap();

    let bitness = get_optimal_bitness(max_value);

    let mut v = BitVector::new();
    for val in indexes.iter() {
        v.push_bits(*val, bitness);
    }

    v.finish_writing(bitness);

    return v.vector;
}

fn get_optimal_bitness(actual_max: usize) -> u8 {
    //This could be made way faster by bitshifting. C code:
    // return (1 << bitness) - 1
    // Either way it is used only once per serialization
    for bitness in 1u8..255u8 {
        let possible_max = (2 as usize).pow(bitness.into());
        if possible_max > actual_max {
            return bitness;
        }
    }
    panic!("This value cannot be stored in up to 255 bytes");
}

pub(crate) fn deserialize(serialized: Vec<u8>) -> Vec<usize> {
    let mut v = BitVector::from(serialized);
    let mut to_return = Vec::new();

    let bitness = v.read_bitness();

    loop {
        if let Some(num) = v.pop_bits(bitness) {
            to_return.push(num);
        } else {
            break;
        }
    }

    to_return.reverse();
    to_return
}

#[cfg(test)]
mod test {
    use super::{deserialize, get_optimal_bitness, serialize, BitVector};

    #[test]
    fn optimal_bitness() {
        let bit2 = 3;
        let bit4 = 15;
        let bit5 = 24;
        let bit9 = 511;

        assert_eq!(get_optimal_bitness(bit2), 2);
        assert_eq!(get_optimal_bitness(bit4), 4);
        assert_eq!(get_optimal_bitness(bit5), 5);
        assert_eq!(get_optimal_bitness(bit9), 9);
    }

    fn test_serialize(arr: Vec<usize>) {
        let serialized = serialize(arr.to_owned());
        let result = deserialize(serialized);

        for element in result.iter().enumerate() {
            assert_eq!(
                *element.1,
                *arr.get(element.0).expect("Arrays aren't the same length")
            );
        }
    }

    #[test]
    fn serialize_8bit() {
        test_serialize(vec![1, 2, 3, 4, 255, 16, 12, 0, 0, 0]);
    }

    #[test]
    fn serialize_9bit() {
        test_serialize(vec![1, 2, 3, 4, 255, 16, 258, 0, 0, 0]);
    }

    #[test]
    fn bitvector_write_byte() {
        let mut v = BitVector::new();
        v.push_bits(1, 1);
        v.push_bits(1, 7);

        assert_eq!(v.vector.len(), 1);
        let byte = *v.vector.get(0).unwrap();

        /* Why 3?
            1. Empty byte = ---- ----

            2. First write_bits (from: 1, how_many: 1):
                Overwrite
                ---- ---1
                ---- ----
              = ---- ---1

            3. Second write_bits (from: 1, how_many: 7):
                Overwrite
                0000 001-
                ---- ---1
              = 0000 0011

            4. 0000 0011 == 3
        */
        assert_eq!(byte, 3);
    }

    #[test]
    fn bitvector_pop_byte() {
        let mut v = BitVector::new();

        // 7 = 0000 0111
        v.push_bits(7, 3);
        v.push_bits(0, 5);
        let zero = v.pop_bits(5).unwrap();

        //now it is     ---- --11
        //and we poped  -- ---1    = 1 (base10)
        let one = v.pop_bits(1).unwrap();

        //now it is     ---- ----
        //and we poped  ---- --11 = 3
        let three = v.pop_bits(2).unwrap();

        //we are writing 3 bits from this --> ---- -101
        //to this --------------------------> ---- ----
        //so as result we should have this -> ---- -101 = 5
        v.push_bits(5, 3);

        assert_eq!(zero, 0);
        assert_eq!(one, 1);
        assert_eq!(three, 3);
        assert_eq!(*v.vector.get(0).unwrap(), 5);
        assert_eq!(v.vector.len(), 1);
    }

    #[test]
    fn bitvector_multi_byte() {
        let mut v = BitVector::new();
        //we will do 0101 1010 1100 0011 = 23235 [90|195 in separate bytes]

        //first byte
        v.push_bits(255, 2);
        v.push_bits(0, 4);
        v.push_bits(255, 2);

        //second byte
        v.push_bits(0, 1);

        //delete "0 1100" = 6 (because we read that from left)
        let twelwe = v.pop_bits(5).unwrap();

        //continue pushing
        v.push_bits(0, 2);
        v.push_bits(255, 2);

        v.push_bits(0, 1);
        v.push_bits(255, 1);
        v.push_bits(0, 1);
        v.push_bits(255, 1);

        v.push_bits(255, 1);
        v.push_bits(0, 1);
        v.push_bits(255, 1);
        v.push_bits(0, 1);

        let ninety = *v.vector.get(1).unwrap();
        let h_ninetyfive = *v.vector.get(0).unwrap();

        assert_eq!(twelwe, 12);
        assert_eq!(ninety, 90);
        assert_eq!(h_ninetyfive, 195);
        assert_eq!(v.vector.len(), 2);

        let bignum = v.pop_bits(16).unwrap();
        assert_eq!(bignum, 23235);
    }
}
