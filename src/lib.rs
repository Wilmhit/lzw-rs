mod lzw;
pub mod serialization;

use std::ffi::CStr;
use std::fs;

const COMPRESS_INPUT_FILENAME: &str = "input.txt";
const COMPRESS_OUTPUT_FILENAME: &str = "output.lzw";
const DECOMPRESS_INPUT_FILENAME: &str = "input.lzw";
const DECOMPRESS_OUTPUT_FILENAME: &str = "output.txt";

#[no_mangle]
pub extern "C" fn compress_input_file() {
    compress_with_filename(COMPRESS_INPUT_FILENAME, COMPRESS_OUTPUT_FILENAME);
}

#[no_mangle]
pub extern "C" fn decompress_input_file() {
    decompress_with_filename(DECOMPRESS_INPUT_FILENAME, DECOMPRESS_OUTPUT_FILENAME);
}

#[no_mangle]
pub extern "C" fn compress_given_file(input: *const u8, output: *const u8) {
    let input_str = c_str_to_str(input);
    let output_str = c_str_to_str(output);
    compress_with_filename(input_str, output_str);
}

#[no_mangle]
pub extern "C" fn decompress_given_file(input: *const u8, output: *const u8) {
    let input_str = c_str_to_str(input);
    let output_str = c_str_to_str(output);
    decompress_with_filename(input_str, output_str);
}

fn c_str_to_str(c_str: *const u8) -> &'static str {
    let to_return: &str;
    unsafe {
        to_return = CStr::from_ptr(c_str as *const i8).to_str().unwrap();
    }
    to_return
}

fn compress_with_filename(input: &str, output: &str) {
    let file_contents = fs::read_to_string(input).expect(&format!("'{}' file not found", input));
    let mut lzw_struct = lzw::LZW::new();
    let mut text = lzw::InputText::from_string(file_contents);
    lzw::init_dictionary(&mut lzw_struct);
    let compressed = lzw::compress_text(&mut lzw_struct, &mut text);
    fs::write(output, compressed).unwrap();
}

fn decompress_with_filename(input: &str, output: &str) {
    let file_contents = fs::read(input).expect(&format!("'{}' file not found", input));
    let decompressed = lzw::decompress_text(file_contents);
    fs::write(output, decompressed).unwrap();
}

#[cfg(test)]
mod test {
    use std::fs;
    use std::process::Command;

    use crate::{COMPRESS_OUTPUT_FILENAME, DECOMPRESS_INPUT_FILENAME};

    use super::{
        compress_input_file, decompress_input_file, COMPRESS_INPUT_FILENAME,
        DECOMPRESS_OUTPUT_FILENAME,
    };

    #[test]
    fn run_extern() {
        if cfg!(target_os = "linux") {
            compress_input_file();

            let _ = Command::new("mv")
                .args(&[COMPRESS_OUTPUT_FILENAME, DECOMPRESS_INPUT_FILENAME])
                .output()
                .unwrap();
            let compressed_size = fs::metadata(DECOMPRESS_INPUT_FILENAME).unwrap().len();
            let raw_size = fs::metadata(COMPRESS_INPUT_FILENAME).unwrap().len();

            decompress_input_file();

            let input = fs::read_to_string(COMPRESS_INPUT_FILENAME).unwrap();
            let output = fs::read_to_string(DECOMPRESS_OUTPUT_FILENAME).unwrap();

            assert!(raw_size > compressed_size);
            assert_eq!(input, output);
        }
    }
}
