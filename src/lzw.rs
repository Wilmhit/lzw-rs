use crate::serialization;
use std::u8;

pub struct LZW {
    dictionary: Vec<Vec<u8>>,
    text: Vec<usize>,
}

impl LZW {
    pub fn new() -> Self {
        LZW {
            dictionary: Vec::new(),
            text: Vec::new(),
        }
    }

    fn from_compressed(text: Vec<usize>) -> Self {
        LZW {
            dictionary: Vec::new(),
            text,
        }
    }

    fn is_in_dictionary(&self, value: Vec<u8>) -> Option<usize> {
        for x in self.dictionary.iter().enumerate() {
            if *x.1 == value {
                return Some(x.0);
            }
        }
        None
    }

    fn get_longest_dic_word_len(&self) -> usize {
        let mut len = 0;
        for x in self.dictionary.iter() {
            if x.len() > len {
                len = x.len();
            }
        }
        len
    }
}

pub struct InputText {
    pub text: Vec<u8>,
}

impl InputText {
    pub fn from_string(text: String) -> InputText {
        InputText {
            text: text.as_bytes().to_vec(),
        }
    }

    fn pop_front(&mut self, how_many: usize) -> Vec<u8> {
        let front: Vec<u8>;
        if how_many > self.text.len() {
            front = self.text.drain(..self.text.len()).collect();
        } else {
            front = self.text.drain(..how_many).collect();
        }
        front
    }

    fn get_front(&self, how_many: usize) -> Vec<u8> {
        if how_many > self.text.len() {
            (&self.text)[..self.text.len()].to_vec()
        } else {
            (&self.text)[..how_many].to_vec()
        }
    }
}

pub fn init_dictionary<'a>(lzw: &mut LZW) {
    for x in 0..256 {
        lzw.dictionary.push((&[x as u8]).to_vec());
    }
}

pub fn compress_text(lzw: &mut LZW, input: &mut InputText) -> Vec<u8> {
    loop {
        if input.text.is_empty() {
            break;
        }

        let max_checking_length = [lzw.get_longest_dic_word_len(), input.text.len()]
            .iter()
            .min()
            .unwrap()
            .to_owned();

        for checking_length in (1..max_checking_length + 1).rev() {
            //This loop is very slow. We should check from begining TODO
            let word = input.get_front(checking_length);

            if let Some(index) = lzw.is_in_dictionary(word.to_owned()) {
                let word_to_add = input.get_front(checking_length + 1);
                lzw.dictionary.push(word_to_add);
                input.pop_front(checking_length);
                lzw.text.push(index);
                break;
            }
        }
    }
    serialization::serialize(lzw.text.to_vec())
}

pub fn decompress_text(text: Vec<u8>) -> Vec<u8> {
    let mut result: Vec<u8> = Vec::new();

    let mut lzw = LZW::from_compressed(serialization::deserialize(text));
    init_dictionary(&mut lzw);

    for vec_index in 0..lzw.text.len() {
        let word_index = lzw.text.get(vec_index).unwrap();
        let word = lzw.dictionary.get(*word_index).unwrap();
        result.append(&mut word.to_owned());

        //Now add (( word + 1 letter from next word )) to dictionary
        if let Some(next_word_index) = lzw.text.get(vec_index + 1) {
            let mut to_add = word.to_owned();

            //if next word should be in dictionary on position last+1 (out of range)
            //that means next word is the word we are creating right now
            if *next_word_index == lzw.dictionary.len() {
                to_add.push(word.to_owned()[0]);
            } else {
                let next_word = lzw.dictionary.get(*next_word_index).unwrap();
                to_add.push(next_word[0]);
            }

            lzw.dictionary.push(to_add);
        }
    }
    result
}

#[cfg(test)]
mod test {
    use crate::lzw::{compress_text, decompress_text, init_dictionary, InputText, LZW};

    fn encode_and_decode(word: String) {
        let mut input = InputText::from_string(word.to_owned());
        let mut lzw = LZW::new();
        init_dictionary(&mut lzw);

        let compressed = compress_text(&mut lzw, &mut input);
        let result = decompress_text(compressed);
        assert_eq!(String::from_utf8(result).unwrap(), word);
    }

    #[test]
    fn encode_and_decode_no_repeat() {
        encode_and_decode("Hello World".into());
    }

    #[test]
    fn encode_and_decode_repeat() {
        encode_and_decode("Hello World Hello World Hello Hello World".into());
    }

    #[test]
    fn encode_and_decode_big_repeat() {
        let mut s = "Hello World ".to_string();
        for _ in 1..10 {
            //TODO crank up this number when compression is faster
            s.push_str(&s.to_owned());
        }
        encode_and_decode(s);
    }
}
